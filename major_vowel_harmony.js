const isHasMajorVowelHarmony = (word) => {
    let kalinMi = false;
    let inceMi = false;
    let kalin = ["a", "ı", "o", "u"];
    let ince = ["e", "i", "ö", "ü"];
    let wordArr = [...word];
    wordArr.map(item => {
        if (kalin.indexOf(item) != -1) {
            kalinMi = true;
        }
        if (ince.indexOf(item) != -1) {
            inceMi = true;
        }
    });
    if (kalinMi & inceMi) return false;
    if (kalinMi != inceMi) return true;
}

export default isHasMajorVowelHarmony;